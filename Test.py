import os
import requests

# OpenAI API key
api_key = os.environ.get('OpenAI_SECRET_KEY')

url = "https://api.openai.com/v1/chat/completions"

headers = {
    "Content-Type": "application/json",
    "Authorization": f"Bearer {api_key}"
}

data = {
    "model": "gpt-4-1106-preview",
    "messages": [
        {
            "role": "system",
            "content": "You are an assistant, and you only reply with JSON."
        },
        {
            "role": "user",
            "content": "Hello!"
        }
    ],
    "response_format": {
        "type": "json_object"
    }
}

response = requests.post(url, headers=headers, json=data)

print(response.json())
