import os
import requests
from sys import stdin
from datetime import datetime


isNewConversation = True

def send_message(api_key, model, messages):
    url = "https://api.openai.com/v1/chat/completions"
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {api_key}"
    }
    data = {
        "model": model,
        "messages": messages,
        "response_format": {
            "type": "json_object"
        }
    }
    response = requests.post(url, headers=headers, json=data)
    return response.json()

# OpenAI API key
api_key = os.environ.get('OpenAI_SECRET_KEY')

# Replace with the desired model (e.g., "gpt-4-1106-preview")
model = "gpt-4-1106-preview"

# Example conversation
conversation = [
    {"role": "system", "content": "You are an assistant, and you only reply with JSON, 請使用繁體中文回應"}
]

print(f"{model}: {{\n    \"response\": \"我會使用繁體中文來回答您的問題。請問您有什麼問題或是需要什麼協助呢？\"\n}}")



for s in stdin:
  if "python -u \"e:\\Bond NKNU\\CPH Lab\\GPT API" in s:
    print("程式執行中")
  else:
    # Extend the conversation with a new user message
    new_user_message = {"role": "user", "content": s}
    conversation.append(new_user_message)
    # print(conversation) #觀察conversation

    # Send another message and receive a response
    response = send_message(api_key, model, conversation)

    # Extract and print the updated assistant's reply
    # print(response)
    updated_assistant_reply = response['choices'][0]['message']['content']
    reply_total_tokens = response['usage']['total_tokens']
    print(f"{model}: {updated_assistant_reply}", end = "")
    print(f" total_tokens: {reply_total_tokens}")

    # 獲取當前時間戳記
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # 將時間, 輸入與輸出寫入文件
    with open('logs/One_way_memory_conversation_log.txt', 'a', encoding='utf-8') as log_file:
        if (isNewConversation == True):
            log_file.write('[NEW CONTINUOUS CONVERSATION]\n')
            isNewConversation = False
        log_file.write(f'[{timestamp}]\n')
        log_file.write(f'InPut: {s}\n')
        log_file.write(f'Output: {model}: {updated_assistant_reply} total_tokens: {reply_total_tokens}\n\n')
